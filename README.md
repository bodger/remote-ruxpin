# remote-ruxpin

Remote control of Teddy Ruxpin

The idea is to have a small program on an Arduino that operates a Teddy Ruxpin
figure's servos on commands from a host.  The servos have separate motors and position sensors, so the Arduino code closes the loop.

The Python code on the host consists of two pieces.  One plays back the audio and receives the motions from analog inputs (could use a joystick or knobs or whatever) and stores them with timestamps in a JSON file.

The other plays back the audio while sending the motion commands back to be implemented.
