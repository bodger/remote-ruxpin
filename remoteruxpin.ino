#include <Wire.h>
#include <Adafruit_MotorShield.h>

#define MOTOR_EYES      1
#define MOTOR_UPPER     2
#define MOTOR_LOWER     3

#define NIOS            3
#define NSERVOS         NIOS

#define DEADBAND        40

struct servo {
  Adafruit_DCMotor *  mp;
  int                 motorno;
  int                 potpin;
  int                 nextpos;
  int                 lastdir;
};

static void updateservo(struct servo * sp);

static Adafruit_MotorShield mots = Adafruit_MotorShield();

static struct servo servos[NSERVOS] = {
  { NULL, MOTOR_EYES,  A1 },
  { NULL, MOTOR_UPPER, A2 },
  { NULL, MOTOR_LOWER, A4 }
};

static bool           led;
static struct servo * se = servos + NSERVOS;
static unsigned long  updatetime;
static int            motor;
static int            dir = FORWARD;
static char           buf[20];
static char *         bp = buf;
static char *         be = buf + 20;

void setup() {
  struct servo *  sp;
  struct io *     ip;

  pinMode(13, OUTPUT);

  Serial.begin(115200);
  mots.begin();

  for (sp = servos; sp < se; ++sp) {
    sp->mp = mots.getMotor(sp->motorno);
    sp->mp->setSpeed(255);
  }
}

void loop() {
  int             pos;
  char            ch;
  struct servo *  sp;
  unsigned long   now;

  while (Serial.available()) {
    ch = Serial.read();

    if (ch == '\n') {
      *bp++ = '\0';
      pos = atoi(buf);
      //Serial.println(pos);

      bp = buf;

      for (sp = servos; sp < se; ++sp) {
        sp->nextpos = pos;
      }
      digitalWrite(13, led);
      led = !led;
    } else {
      *bp++ = ch;
    }
  }

  now = millis();

  if (now > updatetime) {
    for (sp = servos; sp < se; ++sp) {
      updateservo(sp);
    }
    
    updatetime = now + 20;
  }
}

static void updateservo(
  struct servo *  sp)
{
  int             diff;
  int             nextdir;
  int             pos;

  pos = analogRead(sp->potpin);
  diff = pos - sp->nextpos;

  if (sp->motorno == MOTOR_UPPER) {
    diff = -diff;
  }
  
  if (abs(diff) < DEADBAND) {
    nextdir = RELEASE;
  } else {
    if (diff > 0) {
      nextdir = FORWARD;
    } else {
      nextdir = BACKWARD;
    }
  }

#if 0
  if (sp->motorno == MOTOR_UPPER) {
    Serial.print("pos ");
    Serial.print(pos);
    Serial.print(" nextpos ");
    Serial.print(sp->nextpos);
    Serial.print(" diff ");
    Serial.print(diff);
    Serial.print(" nextdir ");
    Serial.println(nextdir);
  }
#endif

  if (sp->lastdir != nextdir) {
#if 0
    Serial.print("motor ");
    Serial.print(sp->motorno);
    Serial.print(" run ");
    Serial.println(nextdir);
#endif
    sp->mp->run(nextdir);
    sp->lastdir = nextdir;
  }
}
