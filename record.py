#!/usr/bin/env python3

import json
import pyaudio
import wave

from telemetrix import telemetrix

PIN_UPPER = 3
wavefile = 'trumpbonk.wav'
updatefile = 'updates.json'
chunk = 1024
reportinterval = 50
maxupdates = 300
deadband = 30

CB_PIN_MODE = 0
CB_PIN = 1
CB_VALUE = 2
CB_TIME = 3

lastdata = None
updates = []
running = True

def mycallback(data):
    global running, lastdata

    if lastdata is None:
        lastdata = data[CB_VALUE]
        return

    diff = data[CB_VALUE] - lastdata

    if abs(diff) > deadband:
        lastdata = data[CB_VALUE]
        updates.append((count, lastdata))

        if (len(updates) % reportinterval) == 0:
            print(len(updates))

        if len(updates) > maxupdates:
            running = False

board = telemetrix.Telemetrix()

board.set_pin_mode_analog_input(PIN_UPPER, callback=mycallback)

wf = wave.open(wavefile, 'rb')
p = pyaudio.PyAudio()

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
    channels=wf.getnchannels(),
    rate=wf.getframerate(),
    output=True)

data = wf.readframes(chunk)
count = 0

while data and running:
    stream.write(data)
    data = wf.readframes(chunk)
    count += 1

with open(updatefile, 'w') as jsonfile:
    json.dump(updates, jsonfile)

print(f'wrote {len(updates)} updates')

stream.close()
p.terminate()
