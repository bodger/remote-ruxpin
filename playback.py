#!/usr/bin/env python3

import json
import pyaudio
import wave
import serial
import time

port = '/dev/cu.usbmodem1411'
wavefile = 'trumpbonk.wav'
updatefile = 'updates.json'
chunk = 1024

ser = serial.Serial(port, 115200, rtscts=False, dsrdtr=False)

print('waiting for serial')

time.sleep(5)

with open(updatefile) as jsonfile:
    updates = json.load(jsonfile)

wf = wave.open(wavefile, 'rb')
p = pyaudio.PyAudio()
index = 0

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
    channels=wf.getnchannels(),
    rate=wf.getframerate(),
    output=True)

data = wf.readframes(chunk)
count = 0

while data:
    stream.write(data)
    if index < len(updates) and count >= updates[index][0]:
        pos = updates[index][1]
        nb = ser.write(f'{pos}\n'.encode())
        ser.flush()
        print(pos, nb)
        index += 1

    data = wf.readframes(chunk)
    count += 1

stream.close()
p.terminate()
